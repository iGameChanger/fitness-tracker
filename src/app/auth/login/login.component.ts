import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    f = new FormGroup({
        'email': new FormControl("", [
            Validators.required,
            Validators.email
        ]),
        'password': new FormControl("", [
            Validators.required,
            Validators.minLength(6)
        ])
    });

    get email() {
        return this.f.get('email');
    }

    get password() {
        return this.f.get('password');
    }

    constructor() { }

    ngOnInit() {
    }

    onSubmit(f) {
        console.log(f);
    }

}
