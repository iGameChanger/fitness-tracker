import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
    
    maxDate;

    constructor() { }

    ngOnInit() {
        this.maxDate = new Date();
        this.maxDate.setDate(this.maxDate.getFullYear() -  18);
    }

    onSubmit(f) {
        console.log(f);
    }

}
